# Module Imports
import mariadb
import sys
conn = None

def connect_db(host,user,password,database_name,port):
    
    # Connect to MariaDB Platform
    try:
        conn = mariadb.connect(
            host=host,
            user=user,
            password=password,
            database=database_name,
            port=port
        )
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        sys.exit(1)
    return True


