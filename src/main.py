from dotenv import load_dotenv
import os

import maria_db

if __name__ == '__main__':
    load_dotenv()
    try:
        maria_db.connect_db(
            os.environ.get("MARIADB_HOST")
            ,os.environ.get("MARIADB_USER")
            ,os.environ.get("MARIADB_PASSWORD")
            ,os.environ.get("MARIADB_DATABASE")
            ,int(os.environ.get("MARIADB_PORT"))
        )
        print(f"mariaDB host: {os.environ.get('MARIADB_HOST')}")
        print("Connected")
    except Exception as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        sys.exit(1)