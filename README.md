# Unittest python

### Set environments

```bash
export MARIADB_HOST=xxx
export MARIADB_ROOT_PASSWORD=xxx
export MARIADB_USER=xxx
export MARIADB_PASSWORD=xxx
export MARIADB_DATABASE=xxx
export MARIADB_PORT=xxx
```

#### Test starter

```bash
python -m unittest test.py
```

#### Install Library

Install MariaDB Connector/C, which is a dependency.

```bash
wget https://dlm.mariadb.com/enterprise-release-helpers/mariadb_repo_setup
sudo apt-get install -y libmariadb3 libmariadb-dev cmake
```

#### Install Library via pip

```bash
pip install -r requirements.txt
```

### Create Dcoker-compose

```bash
envsubst < docker-compose-template.yml > docker-compose.yml
```

### Check Docker-compose format

```bash
docker-compose -f docker-compose.yml config
```

### Start database via docker-compose

```bash
docker-compose up -d
```

### Install mariadb server and client on Debian / Ubuntu (APT)

```bash
sudo apt install -y mariadb-server mariadb-client-10.3
```

### Test connect mariadb command line

```bash
mariadb --host localhost --port xxx \
      --user xxx --password 
```


#### Test connect to database

```bash
python3 -m unittest tests/test_execute_mariadb.py
```

#### Test integration connect to database

```bash
python3 python3 src/main.py
```


