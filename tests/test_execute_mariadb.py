import mariadb

from unittest.mock import MagicMock
import unittest

# parent directory
from src import maria_db

class TestExecuteMariadb(unittest.TestCase):

    def setUp(self):
        self.origin_connect = mariadb.connect

    def tearDown(self):
        mariadb.connect = self.origin_connect

    def test_connected_mariadb(self):
        excepted_connected = True
        mariadb.connect = MagicMock()
        host="127.0.0.1"
        user="maria_user"
        password="maria_password"
        database_name="test"
        port=3307

        actual = maria_db.connect_db(
            host
            ,user
            ,password
            ,database_name
            ,port
        )
        
        self.assertEqual(actual, excepted_connected)
    
    def test_password_is_inconrrect_connected_mariadb(self):
        error_message = Exception("Access denied for user \'maria_user\'@\'127.0.0.1\' (using password: YES)")
        mariadb.connect = MagicMock(side_effect=error_message)
        host="127.0.0.1"
        user="maria_user"
        password="maria_password1"
        database_name="test"
        port=3307
        
        with self.assertRaises(Exception) as context:
            maria_db.connect_db(
                host
                ,user
                ,password
                ,database_name
                ,port
            )

        self.assertRaises(Exception,context)
        
if __name__ == '__main__':
    unittest.main()